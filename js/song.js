export default class Song {
  constructor(title, artist, album, albumImg, filePath, length, player) {
    this.title = title;
    this.artist = artist;
    this.album = album;
    this.albumImg = albumImg;
    this.length = length;
    this.file = filePath;
    this.playing = false;

    const genElement = () => {
      const item = document.createElement('li');
      item.classList.add('playlist-item');

      const songDiv = document.createElement('div');

      const img = document.createElement('img');
      img.src = this.albumImg;

      const infoDiv = document.createElement('div');

      const titleEl = document.createElement('span');
      titleEl.innerHTML = this.title;

      const artistEl = document.createElement('span');
      artistEl.innerHTML = this.artist;

      const play = document.createElement('i');
      play.classList.add('fa', 'fa-play-circle', 'fa-lg');
      play.style.visibility = 'hidden';

      // only show play button on hover
      item.onmouseenter = () => { play.style.visibility = 'visible'; };
      item.onmouseleave = () => { play.style.visibility = 'hidden'; };

      play.onclick = () => {
        player.setSong(this);
        player.play();
      };

      infoDiv.appendChild(titleEl);
      infoDiv.appendChild(artistEl);

      songDiv.appendChild(img);
      songDiv.appendChild(infoDiv);

      item.appendChild(songDiv);
      item.appendChild(play);

      return item;
    };

    this.element = genElement();
  }

  setPlaying(status) {
    this.playing = status;

    if (this.playing && !$(this.element).hasClass('playing-highlight')) {
      $(this.element).addClass('playing-highlight');
    } else {
      $(this.element).removeClass('playing-highlight');
    }
  }
}
