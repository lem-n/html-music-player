import { lengthFormatStr } from './utils';

export default class Player {
  constructor(modal) {
    this.audio = new Audio();
    this.playing = false;
    this.currentSong = null;
    this.volume = 0.5;

    this.shuffle = false;
    this.loop = false;

    this.songs = [];
    this.playlists = new Map();
    this.currentPlaylist = [];

    this.modal = modal;
    this.showModal = true;
    this.modalTimer = 1000 * 60;
    this.continueListeningTimeout = null; // store timeout so we can clear it

    this.load();
  }

  load() {
    this.audio.addEventListener('timeupdate', () => this.updateCurrentTime(), false);
    this.audio.addEventListener('ended', () => this.handleSong(), false);

    // make music pause on modal NO
    this.modal.setClickNo(() => {
      this.pause();
      // eslint-disable-next-line
      console.log('Stopping music playback!');
    });

    this.modal.setClickYes(() => {
      // eslint-disable-next-line
      console.log('Continuing music playback!');
    });

    $('#plr-play').click(() => this.handlePlay());
    $('#plr-forward').click(() => this.forward());
    $('#plr-backward').click(() => this.backward());
    $('#plr-repeat').click(() => this.toggleLoop());
    $('#plr-shuffle').click(() => this.toggleShuffle());
  }

  setModal(show) {
    this.showModal = show;
  }

  // Update the current time in the media player
  updateCurrentTime() {
    $('#plr-current-time').text(lengthFormatStr(this.audio.currentTime));
    // calculate percentage
    const perc = (this.audio.currentTime / this.currentSong.length) * 100;
    $('#plr-song-progress').width(`${perc}%`);
  }

  setSong(song) {
    if (this.currentSong) this.currentSong.setPlaying(false);

    this.currentSong = song;

    const {
      title,
      artist,
      albumImg,
      length
    } = this.currentSong;

    // update elements
    $('#plr-song-title').text(title);
    $('#plr-song-artist').text(artist);
    $('#plr-song-cover').attr('src', albumImg);
    $('#plr-song-length').text(lengthFormatStr(length));

    if (this.playlistHasTrack(this.currentPlaylist, this.currentSong)) {
      $('#plr-favorite').css('color', '#d25571');
    } else {
      $('#plr-favorite').css('color', '#fff');
    }

    this.audio.src = this.currentSong.file;
  }

  setPlaylist(playlist) {
    const list = this.playlists.get(playlist);
    if (list) {
      const listElement = $('#songs')[0];
      $('#playlist').text(playlist);

      this.currentPlaylist = list;
      this.currentPlaylist.forEach((song) => listElement.appendChild(song.element));
    }
  }

  playlistHasTrack() {
    return this.currentPlaylist.findIndex((song) => song.title === this.currentSong.title
      && song.artist === this.currentSong.artist) !== -1;
  }

  play() {
    if (!this.currentSong) return;

    this.audio.volume = this.volume;
    this.audio.play();
    this.playing = true;
    this.currentSong.setPlaying(true);
    $('#plr-play').removeClass('fa-play').addClass('fa-pause');

    if (this.showModal && !this.modal.showing) {
      clearTimeout(this.continueListeningTimeout); // clear old timeout
      this.continueListeningTimeout = setTimeout(() => {
        this.modal.setText('Do you want to continue listening?');
        this.modal.show();
      }, this.modalTimer);
    }
  }

  pause() {
    if (!this.currentSong) return;

    this.audio.pause();
    this.playing = false;
    this.currentSong.setPlaying(false);
    $('#plr-play').removeClass('fa-pause').addClass('fa-play');
  }

  handlePlay() {
    if (this.playing) this.pause();
    else this.play();
  }

  handleSong() {
    if (this.loop) { // reset time and replay song
      this.audio.currentTime = 0;
      this.play();
    } else if (this.shuffle) { // perform basic shuffle (could get same song)
      const nextSong = this.songs[Math.floor(Math.random() * this.songs.length)];
      this.setSong(nextSong);
      this.play();
    } else { // just go to next song
      this.forward();
    }
  }

  forward() {
    if (!this.currentSong) return;

    // add shuffle functionality to forward
    if (this.shuffle) {
      this.handleSong();
      return;
    }

    let nextIndex = this.songs.findIndex((song) => song.title === this.currentSong.title) + 1;
    if (nextIndex >= this.songs.length) nextIndex = 0;
    this.setSong(this.songs[nextIndex]);
    this.play();
  }

  backward() {
    if (!this.currentSong) return;

    if (this.audio.currentTime < 5) {
      let prevIndex = this.songs.findIndex((song) => song.title === this.currentSong.title
        && song.artist === this.currentSong.artist) - 1;

      if (prevIndex === 0) prevIndex = this.songs.length - 1;
      this.setSong(this.songs[prevIndex]);
      this.play();
    } else {
      this.audio.currentTime = 0;
    }
  }

  toggleShuffle() {
    this.shuffle = !this.shuffle;
    this.loop = false;

    if (this.shuffle) {
      $('#plr-shuffle').css('color', '#fff');
      $('#plr-repeat').css('color', '#929da7');
    } else $('#plr-shuffle').css('color', '#929da7');
  }

  toggleLoop() {
    this.loop = !this.loop;
    this.shuffle = false;

    if (this.loop) {
      $('#plr-repeat').css('color', '#fff');
      $('#plr-shuffle').css('color', '#929da7');
    } else $('#plr-repeat').css('color', '#929da7');
  }
}
