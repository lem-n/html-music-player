import { createPlayer, run } from './main';

const player = createPlayer();
run(player);
