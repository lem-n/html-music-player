import loadSongs from './resources';
import Player from './player';
import Modal from './modal';

export function createPlayer() {
  const modal = new Modal();
  const player = new Player(modal);

  const songs = loadSongs(player);
  player.songs = songs;

  return player;
}

/**
 * @param {Player} player player
*/
export function run(player) {
  player.playlists.set('All', player.songs);

  player.setPlaylist('All');
  player.setSong(player.currentPlaylist[Math.floor(Math.random() * player.currentPlaylist.length)]);

  player.setModal(true);
}
