import { getResource } from './utils';
import Song from './song';

export default function loadSongs(player) {
  if (!player) return null;

  // set image paths
  const imgKuka = getResource('images/Kuka_EP-12251.jpg');
  const imgSetec = getResource('images/Ill_Be_Good_EP-12234.jpg');
  const imgAdamAlma = getResource('images/Back_To_The_Sea-8076.jpg');
  const imgEvgeny = getResource('images/Winter_Sunshine_EP-8201.jpg');
  const imgSouvenir = getResource('images/Jeanne_Moreau_EP-11693.jpg');
  const imgIndieArt = getResource('images/Hidup_Itu_Pendek_Seni_Itu_Panjang-7977.jpg');

  // create songs
  const songs = [
    new Song(
      'Things', 'Adam Alma', 'Back To The Sea',
      imgAdamAlma, getResource('songs/Adam__Alma_-_01_-_Things.mp3'), (5 * 60) + 13, player
    ),
    new Song(
      'Winter Sunshine', 'Evgeny Grinko', 'Winter Sunshine',
      imgEvgeny, getResource('songs/Evgeny_Grinko_-_01_-_Winter_Sunshine.mp3'), (3 * 60) + 15, player
    ),
    new Song(
      'Other Child Room', 'Evgeny Grinko', 'Winter Sunshine',
      imgEvgeny, getResource('songs/Evgeny_Grinko_-_02_-_Other_Child_Room.mp3'), (2 * 60) + 10, player
    ),
    new Song(
      'It Smells like Chamomile Tea', 'Evgeny Grinko', 'Winter Sunshine',
      imgEvgeny, getResource('songs/Evgeny_Grinko_-_03_-_It_smells_like_Chamomile_Tea.mp3'), (4 * 60) + 42, player
    ),
    new Song(
      'the operation', 'Kuka', 'Kuka EP',
      imgKuka, getResource('songs/Kuka_-_02_-_the_operation.mp3'), (4 * 60) + 59, player
    ),
    new Song(
      'Vowel Of Owl', 'Setec', 'I\'ll Be Good EP',
      imgSetec, getResource('songs/Setec_-_02_-_Vowel_Of_Owl.mp3'), (2 * 60) + 7, player
    ),
    new Song(
      'Jeanne Moreau', 'Souvenir Driver', 'Jeanne Moreau',
      imgSouvenir, getResource('songs/Souvenir_Driver_-_04_-_Jeanne_Moreau.mp3'), (5 * 60) + 50, player
    ),
    new Song(
      'Cinta Itu Sengit', 'Indie Art Wedding', 'Hidup Itu Pendek Seni Itu Panjang',
      imgIndieArt, getResource('songs/Indie_Art_Wedding_-_02_-_Cinta_Itu_Sengit.mp3'), (4 * 60) + 2, player
    )
  ];

  return songs;
}
