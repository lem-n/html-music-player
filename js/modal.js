export default class Modal {
  constructor() {
    this.element = $('#modal');
    this.modalText = $('#modal-text');
    this.elementYes = $('#modal-yes');
    this.elementNo = $('#modal-no');

    this.showing = false;

    this.defaultBehavior = () => {
      if (this.showing) this.hide();
    };

    this.elementYes.click(this.defaultBehavior);
    this.elementNo.click(this.defaultBehavior);
  }

  setClickYes(fn) {
    this.elementYes.click(() => {
      this.defaultBehavior();
      fn();
    });
  }

  setClickNo(fn) {
    this.elementNo.click(() => {
      this.defaultBehavior();
      fn();
    });
  }

  setText(message) {
    this.modalText.text(message);
  }

  show() {
    this.showing = true;
    $(this.element).removeClass('hidden');
    $('#header').addClass('disabled');
    $('#container').addClass('disabled');
  }

  hide() {
    this.showing = false;
    $(this.element).addClass('hidden');
    $('#header').removeClass('disabled');
    $('#container').removeClass('disabled');
  }
}
